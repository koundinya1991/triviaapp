
import './App.css';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";
import Login from './pages/Login';
import Trivia from './pages/Trivia';
import History from './pages/History';

function App() {
  
  return (
      <Router>
        <Routes>
          <Route path="/login" element={<Login/>}/>
          <Route path="/history" element={<History/>}/>
          <Route path="/trivia" element={<Trivia/>}/>
          {/* //history */}
        </Routes>
      </Router>
    // </div>
  );
}

export default App;
