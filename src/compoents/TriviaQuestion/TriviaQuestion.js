import React, { useEffect, useMemo } from 'react'; 
import { useState } from 'react/cjs/react.development';
import styles from './style.module.scss';
import _shuffle from 'lodash/shuffle';

const TriviaQuestion = ({ question, getSelectedOptionRes}) => {

  const { correct_answer, incorrect_answers = []} = question || {};
  const [selectedOption, setSelectedOption] = useState(null);

  const onOptionClick = (option) => {
    console.log('hello>', option);
    setSelectedOption(option)
  }
  useEffect(() => {
    if(selectedOption === correct_answer) {
      getSelectedOptionRes(true);
    } else {
      getSelectedOptionRes(false);
    }
  }, [selectedOption]);

  const options = useMemo(() => _shuffle([correct_answer, ...incorrect_answers]), [question]);
  const renderOptions = () => {
    return options.map(o =>{ 
      const correctAnsStyle = o === selectedOption ? styles.correct : null;
    
      return <div className={`${styles.option} ${correctAnsStyle}`} onClick={() => onOptionClick(o)}>{o}</div>
    
    })
  };

  // return null;
  return(
    <div className={styles.container}>
      <div className={styles.question}>{question?.question}</div>
      <div className={styles.optionList}>
        {renderOptions()}
      </div>
    </div>
  )
}

export default TriviaQuestion;