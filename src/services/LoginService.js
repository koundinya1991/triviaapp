class LoginService {

  authUser(email, password) {
    const user = this.getUser(email);
    if(this.userExists(user) && this.isPassMatch(user?.password, password)) {
      return true;
    } 
    return false;
  }

  isPassMatch(password, enteredPass) {
    if(password  === enteredPass) {
      return true;
    }
    return false;
  }

  createUser(email, password) {
    window.localStorage.setItem(email, JSON.stringify({email, password}));
  }

  getUser(email) {
    return JSON.parse(window.localStorage.getItem(email));
  }

  userExists(email) {
    const user = this.getUser(email);
    if(JSON.stringify(user)) {
      return true;
    }
    return false;
  }
}

export default new LoginService();