class HistoryService {
  getHistoryByUser(email) {
    const scores = JSON.parse(localStorage.getItem('scoreHistory'));
    const userScores = scores[email];
    return userScores;
  }
}

export default new HistoryService();

