import React, { useCallback, useState } from 'react';
import {Input, Button} from 'antd';

import LoginService from '../../services/LoginService';

import styles from './login.module.scss';
import { useNavigate } from 'react-router-dom';

const DEFAULT_FORM_FIELLDS = {
  email: '',
  value: '',
};

const LoginPage = () => {
  const [formField, setFormFields] = useState(DEFAULT_FORM_FIELLDS);
  const navigate = useNavigate();
    
  const onInputChange = useCallback((field) => (e) => {
    setFormFields((prev) => ({
      ...prev,
      [field]: e.target.value,
    }))
  }, []);

  const onLoginSubmit = () => {
    const { email, password} = formField;
    const authenticatedUser = LoginService.authUser(email, password);
    console.log('the form field', email, password);
    if(authenticatedUser) {
      navigate('/history');
    } else {
      LoginService.createUser(email, password);
      navigate('/trivia')
    }
  }

  return(
    <div className={styles.container}>
      <div className={styles.loginForm}>
        <Input  placeholder="Enter Email" onChange={onInputChange('email')} value={formField['eamil']}/>
        <Input placeholder="Enter password" onChange={onInputChange('password')} value={formField['password']}/>
        <Button onClick={onLoginSubmit}>Log In</Button>
      </div>
    </div>
  );
}

export default LoginPage;