import React, { useEffect, useState, useMemo } from 'react';
import { Button } from 'antd';

import TriviaQuestion from '../../compoents/TriviaQuestion';

import styles from './style.module.scss';

const triviaApi = 'https://opentdb.com/api.php?amount=10&type=multiple';

const Trivia = () => {
  const [questionList, setQuestionsList] = useState([]);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [correctCount, setCorrectCount] = useState(0);
  useEffect(() => {
    (async () => {
      const questions = await fetch(triviaApi).then(res => res.json()).then(res => res?.results);
      console.log(questions);
      setQuestionsList(questions);
    })();
  }, []);
  
  const currentQuestion = useMemo(() => questionList[currentQuestionIndex], [currentQuestionIndex, questionList]);
  
  const handleNextClick = () => {
    setCurrentQuestionIndex(currentQuestionIndex+1);
  }
  
  const handleSelectedOptionRes = (res) => {
    if(res) {
      setCorrectCount(correctCount+1);
    }
  }
  const count = questionList.length === currentQuestionIndex ? `${correctCount}/${questionList.length}` : <div>finish quiz to get res</div>

  return(
    <div className={styles.container}>
      <div className={styles.toolbar}>{count} </div>
      <div className={styles.questionContainer}>
        <TriviaQuestion question={currentQuestion} getSelectedOptionRes={handleSelectedOptionRes}/>
      </div>
      <Button className={styles.next} onClick={handleNextClick}> next</Button>
    </div>
  )
}

export default Trivia;